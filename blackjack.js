const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
function CardPlayer(name) {
  // CREATE FUNCTION HERE
    this.name = name;
    this.hand = []; 
    this.drawCard = function (){
      let updateHand = blackjackDeck[Math.floor(Math.random() * 52)]; 
      this.hand.push(updateHand); 
    } 
}



// CREATE TWO NEW CardPlayers
let dealer = new CardPlayer('Dealer');  
let player = new CardPlayer('Player'); 

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} - Object containing Total points and whether hand isSoft
 */
const calcPoints = function(hand) {
  // CREATE FUNCTION HERE
  let total = 0; 
  let isSoft = false; 
  let aceCount = 0;
  for (let i = 0; i < hand.length; i++){
    total += hand[i].val; 
    if (hand[i].displayVal === 'Ace') {
      ++aceCount; 
    }
  }//end for loop 

  while(total>21 && aceCount > 0 ){
    total -= 10;
    aceCount--; 
    isSoft = true;
  }
  return {
    total: total,
    isSoft: isSoft
  }; 
}//end calcPoints

/**
 * Determines whether the dealer should draw another card
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = function(dealerHand) {
  // CREATE FUNCTION HERE
   let dealerPoints = calcPoints(dealerHand); 
   if (dealerPoints.total < 16) {
      return true; 
   }else if (dealerPoints.total === 17 && dealerPoints.isSoft == true ){
      return true; 
   }else{
     return false; 
   }
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} States the player's score, the dealer's score, and who wins
 */
const determineWinner = function(playerScore, dealerScore) {
  // CREATE FUNCTION HERE
    if (playerScore == dealerScore){
      return `Dealer Score: ${dealerScore}, Player Score:${playerScore} Tie Game!`; 
    } else if (playerScore > 21) {
      return `Dealer Score: ${dealerScore}, Player Score:${playerScore}. Player busts. Dealer wins.`; 
    }else if (playerScore > dealerScore) {
      return `Dealer Score: ${dealerScore}, Player Score: ${playerScore}. Player Wins.`; 
    }else{
      return `Dealer Score: ${dealerScore}, Player Score:${playerScore}. Dealer Wins.`; 
    }

}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = function(count, dealerCard) {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = function(player) {
  let displayHand = player.hand.map(function(card) { return card.displayVal});
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  // IMPLEMENT DEALER LOGIC BELOW
  let dealerScore = dealerShouldDraw(dealer.hand); 
  

  return determineWinner(playerScore, dealerScore);
}
console.log(startGame());