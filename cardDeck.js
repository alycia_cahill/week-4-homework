/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck = function() {
  // Write function HERE
  	let cards = [ ]; //creates emtpy array 
  	const suits = ['hearts', 'diamonds', 'spades', 'clubs']; 

  	for (let i=0; i< suits.length;  i++){
  		for (let j=2; j<=10; j++){
  			cards.push({
  				val: j, 
  				displayVal: j.toString(), 
  				suits: suits[i]
  			}); 
  		}//end for loop 
  		cards.push({
  			val: 10, 
  			displayVal: 'Jack', 
  			suits: suits[i]
  		}); 
  		cards.push({
  			val: 10, 
  			displayVal: 'Queen', 
  			suits: suits[i]
  		}); 
  		cards.push({
  			val: 10, 
  			displayVal: 'King', 
  			suits: suits[i] 
  		}); 
  		cards.push({
  			val: 11, 
  			displayVal: 'Ace', 
  			suits: suits[i]
  		}); 
  	}//end for loop 
return cards; 
}// End get deck



// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard && 
    randomCard.displayVal && 
    typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);